�}q (X   docqX�  This module provides mechanisms to use signal handlers in Python.

Functions:

alarm() -- cause SIGALRM after a specified time [Unix only]
setitimer() -- cause a signal (described below) after a specified
               float time and the timer may restart then [Unix only]
getitimer() -- get current value of timer [Unix only]
signal() -- set the action for a given signal
getsignal() -- get the signal action for a given signal
pause() -- wait until a signal arrives [Unix only]
default_int_handler() -- default SIGINT handler

signal constants:
SIG_DFL -- used to refer to the system default handler
SIG_IGN -- used to ignore the signal
NSIG -- number of defined signals
SIGINT, SIGTERM, etc. -- signal numbers

itimer constants:
ITIMER_REAL -- decrements in real time, and delivers SIGALRM upon
               expiration
ITIMER_VIRTUAL -- decrements only when the process is executing,
               and delivers SIGVTALRM upon expiration
ITIMER_PROF -- decrements both when the process is executing and
               when the system is executing on behalf of the process.
               Coupled with ITIMER_VIRTUAL, this timer is usually
               used to profile the time spent by the application
               in user and kernel space. SIGPROF is delivered upon
               expiration.


*** IMPORTANT NOTICE ***
A signal handler function is called with two arguments:
the first is the signal number, the second is the interrupted stack frame.qX   membersq}q(X   SIGINTq}q(X   kindqX   dataqX   valueq	}q
X   typeq]qX   builtinsqX   intq�qasuX   SIG_IGNq}q(hhh	}qh]qhasuX   __name__q}q(hhh	}qh]qhX   strq�qasuX   __package__q}q(hhh	}qh]qhasuX   CTRL_C_EVENTq}q(hhh	}q h]q!hasuX   signalq"}q#(hX   functionq$h	}q%(hXQ  Set the action for the given signal.

The action can be SIG_DFL, SIG_IGN, or a callable Python object.
The previous action is returned.  See getsignal() for possible return values.

*** IMPORTANT NOTICE ***
A signal handler function is called with two arguments:
the first is the signal number, the second is the interrupted stack frame.q&X	   overloadsq']q(}q)(X   argsq*}q+(X   nameq,h*X
   arg_formatq-X   *q.u}q/(h,X   kwargsq0h-X   **q1u�q2hXQ  Set the action for the given signal.

The action can be SIG_DFL, SIG_IGN, or a callable Python object.
The previous action is returned.  See getsignal() for possible return values.

*** IMPORTANT NOTICE ***
A signal handler function is called with two arguments:
the first is the signal number, the second is the interrupted stack frame.q3uauuX   SIGABRTq4}q5(hhh	}q6h]q7hasuX   NSIGq8}q9(hhh	}q:h]q;hasuX   SIGFPEq<}q=(hhh	}q>h]q?hasuX	   getsignalq@}qA(hh$h	}qB(hX$  Return the current action for the given signal.

The return value can be:
  SIG_IGN -- if the signal is being ignored
  SIG_DFL -- if the default action for the signal is in effect
  None    -- if an unknown handler is in effect
  anything else -- the callable Python object used as a handlerqCh']qD}qE(h*}qF(h,h*h-h.u}qG(h,h0h-h1u�qHhX$  Return the current action for the given signal.

The return value can be:
  SIG_IGN -- if the signal is being ignored
  SIG_DFL -- if the default action for the signal is in effect
  None    -- if an unknown handler is in effect
  anything else -- the callable Python object used as a handlerqIuauuX   SIGSEGVqJ}qK(hhh	}qLh]qMhasuX   SIGTERMqN}qO(hhh	}qPh]qQhasuX   set_wakeup_fdqR}qS(hh$h	}qT(hX�   set_wakeup_fd(fd) -> fd

Sets the fd to be written to (with the signal number) when a signal
comes in.  A library can use this to wakeup select or poll.
The previous fd or -1 is returned.

The fd must be non-blocking.qUh']qV}qW(h*}qXh,X   fdqYs�qZhX�   Sets the fd to be written to (with the signal number) when a signal
comes in.  A library can use this to wakeup select or poll.
The previous fd or -1 is returned.

The fd must be non-blocking.q[X   ret_typeq\]q]hX   intq^�q_auauuX
   __loader__q`}qa(hX   typerefqbh	]qcX   _frozen_importlibqdX   BuiltinImporterqe�qfauX   CTRL_BREAK_EVENTqg}qh(hhh	}qih]qjhasuX   SIGBREAKqk}ql(hhh	}qmh]qnhasuX   __doc__qo}qp(hhh	}qqh]qrhasuX   __spec__qs}qt(hhh	}quh]qvhdX
   ModuleSpecqw�qxasuX   SIG_DFLqy}qz(hhh	}q{h]q|hasuX   SIGILLq}}q~(hhh	}qh]q�hasuX   default_int_handlerq�}q�(hh$h	}q�(hXj   default_int_handler(...)

The default handler for SIGINT installed by Python.
It raises KeyboardInterrupt.q�h']q�}q�(h*}q�(h,h*h-h.u�q�hXP   The default handler for SIGINT installed by Python.
It raises KeyboardInterrupt.q�uauuhe}q�(hhh	}q�(X   mroq�]q�(hfhX   objectq��q�eX   basesq�]q�h�ahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    q�X	   is_hiddenq��h}q�(X   __new__q�}q�(hh$h	}q�(hXG   Create and return a new object.  See help(type) for accurate signature.q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hXG   Create and return a new object.  See help(type) for accurate signature.q�uauuX	   find_specq�}q�(hhh	}q�h]q�hX   methodq��q�asuX   __str__q�}q�(hX   methodq�h	}q�(hX   Return str(self).q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hX   Return str(self).q�uauuX   __ge__q�}q�(hh�h	}q�(hX   Return self>=value.q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hX   Return self>=value.q�uauuX   __dir__q�}q�(hh�h	}q�(hX.   __dir__() -> list
default dir() implementationq�h']q�}q�(h*}q�(h]q�hX   objectqq�ah,X   selfq�u�q�hX   default dir() implementationq�h\]q�hX   listqȆq�auauuX   __lt__q�}q�(hh�h	}q�(hX   Return self<value.q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hX   Return self<value.q�uauuX
   is_packageq�}q�(hh$h	}q�(hX4   Return False as built-in modules are never packages.q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hX4   Return False as built-in modules are never packages.q�uauuX   __gt__q�}q�(hh�h	}q�(hX   Return self>value.q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hX   Return self>value.q�uauuX
   __reduce__q�}q�(hh�h	}q�(hX   helper for pickleq�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�q�hX   helper for pickleq�uauuX   __dict__q�}q�(hhh	}q�h]q�hX   mappingproxyq��q�asuX   __subclasshook__q�}q�(hh$h	}q�(hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
q�h']q�}q�(h*}q�(h,h*h-h.u}q�(h,h0h-h1u�r   hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r  uauuX
   get_sourcer  }r  (hh$h	}r  (hX8   Return None as built-in modules do not have source code.r  h']r  }r  (h*}r  (h,h*h-h.u}r	  (h,h0h-h1u�r
  hX8   Return None as built-in modules do not have source code.r  uauuX   __hash__r  }r  (hh�h	}r  (hX   Return hash(self).r  h']r  }r  (h*}r  (h,h*h-h.u}r  (h,h0h-h1u�r  hX   Return hash(self).r  uauuX   __delattr__r  }r  (hh�h	}r  (hX   Implement delattr(self, name).r  h']r  }r  (h*}r  (h,h*h-h.u}r  (h,h0h-h1u�r  hX   Implement delattr(self, name).r  uauuX
   __sizeof__r   }r!  (hh�h	}r"  (hX6   __sizeof__() -> int
size of object in memory, in bytesr#  h']r$  }r%  (h*}r&  (h]r'  h�ah,h�u�r(  hX"   size of object in memory, in bytesr)  h\]r*  h_auauuX
   __module__r+  }r,  (hhh	}r-  h]r.  hasuX   __ne__r/  }r0  (hh�h	}r1  (hX   Return self!=value.r2  h']r3  }r4  (h*}r5  (h,h*h-h.u}r6  (h,h0h-h1u�r7  hX   Return self!=value.r8  uauuX   __reduce_ex__r9  }r:  (hh�h	}r;  (hX   helper for pickler<  h']r=  }r>  (h*}r?  (h,h*h-h.u}r@  (h,h0h-h1u�rA  hX   helper for picklerB  uauuX   get_coderC  }rD  (hh$h	}rE  (hX9   Return None as built-in modules do not have code objects.rF  h']rG  }rH  (h*}rI  (h,h*h-h.u}rJ  (h,h0h-h1u�rK  hX9   Return None as built-in modules do not have code objects.rL  uauuX   create_modulerM  }rN  (hh$h	}rO  (hX   Create a built-in modulerP  h']rQ  }rR  (h*}rS  (h,h*h-h.u}rT  (h,h0h-h1u�rU  hX   Create a built-in modulerV  uauuX   __le__rW  }rX  (hh�h	}rY  (hX   Return self<=value.rZ  h']r[  }r\  (h*}r]  (h,h*h-h.u}r^  (h,h0h-h1u�r_  hX   Return self<=value.r`  uauuX   exec_modulera  }rb  (hh$h	}rc  (hX   Exec a built-in modulerd  h']re  }rf  (h*}rg  (h,h*h-h.u}rh  (h,h0h-h1u�ri  hX   Exec a built-in modulerj  uauuX
   __format__rk  }rl  (hh�h	}rm  (hX   default object formatterrn  h']ro  }rp  (h*}rq  (h,h*h-h.u}rr  (h,h0h-h1u�rs  hX   default object formatterrt  uauuX   __setattr__ru  }rv  (hh�h	}rw  (hX%   Implement setattr(self, name, value).rx  h']ry  }rz  (h*}r{  (h,h*h-h.u}r|  (h,h0h-h1u�r}  hX%   Implement setattr(self, name, value).r~  uauuX   find_moduler  }r�  (hh$h	}r�  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  uauuX	   __class__r�  }r�  (hhbh	]r�  hX   typer�  �r�  auX   __repr__r�  }r�  (hh�h	}r�  (hX   Return repr(self).r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hX   Return repr(self).r�  uauuX   __init_subclass__r�  }r�  (hh$h	}r�  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  uauuX   __eq__r�  }r�  (hh�h	}r�  (hX   Return self==value.r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hX   Return self==value.r�  uauuX   module_reprr�  }r�  (hh$h	}r�  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r�  uauuX   load_moduler�  }r�  (hh$h	}r�  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  uauuho}r�  (hhh	}r�  h]r�  hasuX   __init__r�  }r�  (hh�h	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h']r�  }r�  (h*}r�  (h,h*h-h.u}r�  (h,h0h-h1u�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r�  uauuX   __weakref__r�  }r�  (hX   propertyr�  h	}r�  (hX2   list of weak references to the object (if defined)r�  h]r�  h�auuuuuuu.