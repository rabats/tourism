"""
Definition of urls for Tourism.
"""

from datetime import datetime
from django.conf.urls import url
import django.contrib.auth.views
from django.conf.urls import include
import app.forms
import app.views
import django
from django.utils.translation import ugettext_lazy as _
from django.conf.urls.i18n import i18n_patterns
# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    
]
urlpatterns += i18n_patterns(
    # Examples:
    url(r'^$', app.views.home, name='home'),
    url(r'^about1', app.views.about1, name='about1'),
    url(r'^atlas/', app.views.atlas, name='atlas'),
    url(r'^infoatlas/', app.views.infoatlas, name='infoatlas'),
    url(r'^tourists/', app.views.tourists, name='tourists'),
    url(r'^restaurants/', app.views.restaurants, name='restaurants'),
    url(r'^regatlas/(?P<pk>[\w|\W]*)/$', app.views.regatlas, name='regatlas'),
    url(r'^top5/(?P<pk>[\w|\W]*)', app.views.top5, name='top5'),
    url(r'^digest/(?P<pk>[\w|\W]*)', app.views.digest, name='digest'),
    url(r'^objects/(?P<pk>[\w|\W]*)/(?P<dist>[\w|\W]*)/$', app.views.objects, name='objects'),
    url(r'^map/(?P<pk>[\w|\W]*)', app.views.map, name='map'),
    #url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^login/$',
         django.contrib.auth.views.LoginView.as_view(template_name='app/login.html'),
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
            {
                'title': 'Log in',
                'year': datetime.now().year,
            }
        },
        name='login'),
    url(r'^logout$',
       django.contrib.auth.views.LogoutView.as_view(),
        {
            'next_page': '/',
        },
        name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', admin.site.urls),
    
)
