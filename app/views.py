"""
Definition of views.
"""
from django.views.generic import ListView, CreateView, UpdateView
from django.urls import reverse_lazy
from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from django.template import RequestContext
from datetime import datetime
from app.models import *
from app.forms import *
from django.forms import modelform_factory
import os
def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    lang = request.LANGUAGE_CODE
    print(lang)
    return render(
        request,
        
        'app/index.html',
        {
           'lg':lang,
           #'title':'Home Page',
            #'year':datetime.now().year,
        }
    )
#Atlas regionov
def regatlas(request,  pk):
    lang = request.LANGUAGE_CODE
    rn = Region_name.objects.get(name_ru=pk)
    r = Region.objects.get(name_id=rn.id)
    if lang == 'en':
        name = 'name_en'
    elif lang == 'kz':
        name = 'name_kz'
    else:
        name = 'name_ru'
    n = Region_name.objects.all().order_by(name)
    n1 = n.exclude(name_ru='город Алматы')
    n1 = n1.exclude(name_ru='город Астана')
   
    r1 = Region.objects.all()
    rb = Region_borders.objects.all()
    ra = Region_administration.objects.all()
    rd = Region_directory.objects.all()
    dn = District_n.objects.all()
    d = District.objects.all()
    d = d.filter(region_id=r.id)
    url= '/app/img/objects/'
    o = Object.objects.all()[:5]
    on = Objects_name.objects.all()[:5]
    ot = Object_type.objects.all() 
    oa = Objects_about.objects.all()[:5]
    ol = Objects_location.objects.all()[:5]
    t = Tourism_type.objects.all()
    
    return render(
        request,
        'app/regatlas.html',
        {
            'url':url,
            'pk': pk,
            'r': r, 'rn': rn, 'lg': lang,
            'region':r1,
            'name': rn,
            'names': n1,
            'border': rb,
            'admin': ra,
            'sp': rd,
            'd':d, 'dn': dn,
            'obj':o, 'on':on, 'ot':ot,'oa':oa,'ol':ol,'t':t,
    }
    )

def top5(request, pk):
    assert isinstance(request, HttpRequest)
    lang = request.LANGUAGE_CODE
    n = Region_name.objects.all()
    rn = Region_name.objects.get(name_ru=pk)
    r = Region.objects.get(name_id=rn.id)
    r1 = Region.objects.all()
    rb = Region_borders.objects.all()
    ra = Region_administration.objects.all()
    rd = Region_directory.objects.all()
    dn = District_n.objects.all()
    d = District.objects.all()
    d = d.filter(region_id=r.id)
    url= '/app/img/objects/'
    o = Object.objects.all()[:5]
    on = Objects_name.objects.all()[:5]
    ot = Object_type.objects.all() 
    oa = Objects_about.objects.all()[:5]
    ol = Objects_location.objects.all()[:5]
    t = Tourism_type.objects.all()

    return render(
        request,
        'app/top5.html',
        {
            'url':url, 
            'lg': lang,
            'pk': pk,
            'r': r,
            'region':r1,
            'name': rn,
            'names': n,
            'border': rb,
            'admin': ra,
            'sp': rd,
            'd':d, 'dn': dn,
            'obj':o, 'on':on, 'ot':ot,'oa':oa,'ol':ol,'t':t,
    }
    )

def digest(request,pk):
    lang = request.LANGUAGE_CODE
    base = 'app/layout.html'
    if request.method == "GET":
        return render(request, 'app/digest.html',{
                                                'base':base, 'pk': pk, 'lg': lang, 
             }
    )

def objects(request, pk, dist):
    lang = request.LANGUAGE_CODE
    if lang == 'en':
        name = 'name_en'
    elif lang == 'kz':
        name = 'name_kz'
    else:
        name = 'name_ru'
    n = Region_name.objects.all().order_by(name)
    n1 = n.exclude(name_ru='город Алматы')
    n1 = n1.exclude(name_ru='город Астана')
    o = Object.objects.all()
    rn = Region_name.objects.get(name_ru=pk)
    o = o.filter(region_id=rn.id)
    if dist != '':
        o = o.filter(district_id=dist)
    k = District.objects.all().filter(region_id=rn.id)
    kn = District_n.objects.order_by(name)
    city = {}
    for i in k:
        if 'город' in i.district.name_ru:
            city[i.district_id]=[i.district.name_ru,i.district.name_kz,i.district.name_en,]
    for i in city:
        for y in city[i]:
            kn = kn.exclude(name_ru=y)
        print(city[i])
    on = Objects_name.objects.all()
    ot = Object_type.objects.all() 
    oa = Objects_about.objects.all()
    ol = Objects_location.objects.all()
    t = Tourism_type.objects.all()
    url= '/app/img/objects/'  
    return render(request, 'app/objects.html',{ 'lg': lang, 'rn':rn,
          'names': n1, 'k':k,'kn':kn, 'city': city,
          'url':url,'pk': pk,'dist': dist,'obj': o,
          'on':on, 'ot':ot,'oa':oa,'ol':ol,'t':t,
   }) 

def map(request,pk):
    lang = request.LANGUAGE_CODE
    n = Region_name.objects.all()
    n1 = n.exclude(name_ru='город Алматы')
    n1 = n1.exclude(name_ru='город Астана')
    return render(
        request,
        'app/map.html',
        {           
            'lg': lang,
            'pk': pk,            
            'names': n1,
            }
    )

def atlas(request):    
    lang = request.LANGUAGE_CODE
    base = 'app/layout.html'
    if request.method == "GET":
        return render(request, 'app/atlas.html',{
                                                'lg':lang,
                                                'base':base,
             }
    )

def infoatlas(request):
    lang = request.LANGUAGE_CODE
    base = 'app/layout.html'
    if request.method == "GET":
        return render(request, 'app/infoatlas.html',{ 
                                                     'lg':lang,
                                                     'base': base,} )

def about1(request):
    lang = request.LANGUAGE_CODE
    base = 'app/layout.html'
    if request.method == "GET":
        return render(request, 'app/about1.html',{
                                                    'lg':lang,
                                                    'base':base, } )


def tourists(request):
    lang = request.LANGUAGE_CODE
    base = 'app/layout.html'
    ha = Hotel_about.objects.all()
    return render(request, 'app/tourists.html',{
                                                    'lg':lang,
                                                    'base':base,
                                                    'ha':ha} )

def restaurants(request):
    lang = request.LANGUAGE_CODE
    base = 'app/layout.html'
    ha = Restaurant.objects.all()
    return render(request, 'app/restaurants.html',{
                                                    'lg':lang,
                                                    'base':base,
                                                    'ha':ha} )