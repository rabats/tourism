# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-09-26 03:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20180907_1652'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='district',
            options={'verbose_name': 'Описание района', 'verbose_name_plural': 'Описание района'},
        ),
        migrations.AlterModelOptions(
            name='district_n',
            options={'ordering': ['name_ru'], 'verbose_name': 'Районы', 'verbose_name_plural': 'Районы'},
        ),
        migrations.AlterModelOptions(
            name='object',
            options={'verbose_name': 'Объекты', 'verbose_name_plural': 'Объекты'},
        ),
        migrations.AlterModelOptions(
            name='object_type',
            options={'verbose_name': 'Тип объекта', 'verbose_name_plural': 'Тип объекта'},
        ),
        migrations.AlterModelOptions(
            name='objects_about',
            options={'verbose_name': 'Об объекте', 'verbose_name_plural': 'Об объекте'},
        ),
        migrations.AlterModelOptions(
            name='objects_location',
            options={'verbose_name': 'Местоположение объекта', 'verbose_name_plural': 'Местоположение объекта'},
        ),
        migrations.AlterModelOptions(
            name='objects_name',
            options={'verbose_name': 'Название объекта', 'verbose_name_plural': 'Название объекта'},
        ),
        migrations.AlterModelOptions(
            name='region',
            options={'verbose_name': 'Паспорт региона', 'verbose_name_plural': 'Паспорт региона'},
        ),
        migrations.AlterModelOptions(
            name='region_administration',
            options={'verbose_name': 'Администрация региона', 'verbose_name_plural': 'Администрация региона'},
        ),
        migrations.AlterModelOptions(
            name='region_borders',
            options={'verbose_name': 'Границы региона', 'verbose_name_plural': 'Границы региона'},
        ),
        migrations.AlterModelOptions(
            name='region_directory',
            options={'verbose_name': 'Справка региона', 'verbose_name_plural': 'Справка региона'},
        ),
        migrations.AlterModelOptions(
            name='region_events',
            options={'verbose_name': 'События в регионе', 'verbose_name_plural': 'События в регионе'},
        ),
        migrations.AlterModelOptions(
            name='region_name',
            options={'verbose_name': 'Регион', 'verbose_name_plural': 'Регионы'},
        ),
        migrations.AlterModelOptions(
            name='region_weather',
            options={'verbose_name': 'Погода в регионе', 'verbose_name_plural': 'Погода в регионе'},
        ),
        migrations.AlterModelOptions(
            name='tourism_type',
            options={'verbose_name': 'Тип туризма', 'verbose_name_plural': 'Тип туризма'},
        ),
    ]
