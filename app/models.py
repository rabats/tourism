from django.db import models
from django import forms
from django.forms import ModelForm
#REGION
class Month_name(models.Model):
    name_kz = models.TextField(max_length = 100, null = True)
    name_ru = models.TextField(max_length = 100, null = True)
    name_en = models.TextField(max_length = 100, null = True)
class Region_name(models.Model):
    name_kz = models.TextField(max_length = 100, null = True)
    name_ru = models.TextField(max_length = 100, null = True)
    name_en = models.TextField(max_length = 100, null = True)
    class Meta:
      
        verbose_name = "Регион"
        verbose_name_plural = "Регионы"
    def __str__(self):
        return self.name_ru

class Region(models.Model):
    name = models.ForeignKey(Region_name, related_name="regions",on_delete=models.CASCADE)
    population = models.IntegerField(null = True)
    territory = models.FloatField(max_length = 100, null = True)
    map = models.URLField(max_length = 1000, null = True)
    site = models.URLField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Паспорт региона"
        verbose_name_plural = "Паспорт региона"
    def __str__(self):
        return self.name.name_ru

class Region_borders(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    borders_kz = models.TextField(max_length = 1000, null = True)
    borders_ru = models.TextField(max_length = 1000, null = True)
    borders_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Границы региона"
        verbose_name_plural = "Границы региона"
    def __str__(self):
        return self.region.name.name_ru
 
class Region_administration(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    admin_kz = models.TextField(max_length = 1000, null = True)
    admin_ru = models.TextField(max_length = 1000, null = True)
    admin_en = models.TextField(max_length = 1000, null = True)
    population = models.IntegerField()
    class Meta:
        verbose_name = "Администрация региона"
        verbose_name_plural = "Администрация региона"
    def __str__(self):
        return self.admin_ru

class Region_directory(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    directory_kz = models.TextField(max_length = 1000, null = True)
    directory_ru = models.TextField(max_length = 1000, null = True)
    directory_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Справка региона"
        verbose_name_plural = "Справка региона"
    def __str__(self):
        return self.region.name.name_ru

class Region_events(models.Model):
    region =  models.ForeignKey(Region, on_delete=models.CASCADE)
    description_kz = models.TextField(max_length = 1000, null = True)
    description_ru = models.TextField(max_length = 1000, null = True)
    description_en = models.TextField(max_length = 1000, null = True)
    origin = models.TextField(max_length = 10000,null = True)
    class Meta:
        verbose_name = "События в регионе"
        verbose_name_plural = "События в регионе"
    def __str__(self):
        return self.description_ru

class Region_weather(models.Model):
    region = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    month_id = models.ForeignKey(Month_name, on_delete=models.CASCADE)
    temperature = models.FloatField()
    recommendation_kz = models.TextField(max_length = 1000, null = True)
    recommendation_ru = models.TextField(max_length = 1000, null = True)
    recommendation_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Погода в регионе"
        verbose_name_plural = "Погода в регионе"
    def __str__(self):
        return self.region.name_ru

#District
class District_n(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        ordering = ['name_ru']
        verbose_name = "Районы"
        verbose_name_plural = "Районы"
    def __str__(self):
        return self.name_ru

class District(models.Model):
    district = models.ForeignKey(District_n, related_name="districts", on_delete=models.CASCADE,null = True)
    region = models.ForeignKey(Region_name, on_delete=models.CASCADE,null = True)
    description_kz = models.TextField(max_length = 1000, null = True)
    description_ru = models.TextField(max_length = 1000,null = True)
    description_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Описание района"
        verbose_name_plural = "Описание района"
    def __str__(self):
        return self.district.name_ru
    
#City
class City_name(models.Model):
    name_kz = models.TextField(max_length = 100, null = True)
    name_ru = models.TextField(max_length = 100, null = True)
    name_en = models.TextField(max_length = 100, null = True)
    
#Object
class Tourism_type(models.Model):
    type_kz = models.TextField(max_length = 100, null = True)
    type_ru = models.TextField(max_length = 100, null = True)
    type_en = models.TextField(max_length = 100, null = True)
    class Meta:
        verbose_name = "Тип туризма"
        verbose_name_plural = "Тип туризма"
    def __str__(self):
        return self.type_ru

class Object_type(models.Model):
    type_kz = models.TextField(max_length = 100, null = True)
    type_ru = models.TextField(max_length = 100, null = True)
    type_en = models.TextField(max_length = 100, null = True)
    class Meta:
        verbose_name = "Тип объекта"
        verbose_name_plural = "Тип объекта"
    def __str__(self):
        return self.type_ru

class Objects_name(models.Model):
    name_kz = models.TextField(max_length = 100, null = True)
    name_ru = models.TextField(max_length = 100, null = True)
    name_en = models.TextField(max_length = 100, null = True)
    class Meta:
        verbose_name = "Название объекта"
        verbose_name_plural = "Название объекта"
    def __str__(self):
        return self.name_ru

class Object(models.Model):
    region = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    district = models.ForeignKey(District_n, on_delete=models.CASCADE)
    tourism_type = models.ForeignKey(Tourism_type, on_delete=models.CASCADE)
    object = models.ForeignKey(Objects_name, on_delete=models.CASCADE)
    object_type = models.ForeignKey(Object_type, on_delete=models.CASCADE)
    description_kz = models.TextField(max_length = 1000, null = True)
    description_ru = models.TextField(max_length = 1000, null = True)
    description_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Объекты"
        verbose_name_plural = "Объекты"
    def __str__(self):
        return self.object.name_ru

class Objects_location(models.Model):
    object = models.ForeignKey(Object, on_delete=models.CASCADE)
    location_kz = models.TextField(max_length = 1000, null = True)
    location_ru = models.TextField(max_length = 1000, null = True)
    location_en = models.TextField(max_length = 1000, null = True)
    map = models.URLField(max_length = 10000,null = True)
    route = models.TextField(max_length = 10000,null = True)
    class Meta:
        verbose_name = "Местоположение объекта"
        verbose_name_plural = "Местоположение объекта"
    def __str__(self):
        return self.object.object.name_ru

class Objects_about(models.Model):
    object = models.ForeignKey(Object, on_delete=models.CASCADE)
    tag_kz = models.TextField(max_length = 1000, null = True)
    tag_ru = models.TextField(max_length = 1000, null = True)
    tag_en = models.TextField(max_length = 1000, null = True)
    origin = models.TextField(max_length = 10000,null = True)
    author_kz = models.TextField(max_length = 1000,null = True)
    author_ru = models.TextField(max_length = 1000,null = True)
    author_en = models.TextField(max_length = 1000,null = True)
    photo = models.ImageField(null = True)
    class Meta:
        verbose_name = "Об объекте"
        verbose_name_plural = "Об объекте"
    def __str__(self):
        return self.object.object.name_ru


#ITC
class ITC_name(models.Model):
    name_kz = models.TextField(max_length = 100, null = True)
    name_ru = models.TextField(max_length = 100, null = True)
    name_en = models.TextField(max_length = 100, null = True)
class ITC1(models.Model):
    name = models.ForeignKey(ITC_name, on_delete=models.CASCADE)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    contacts = models.TextField(max_length=100, null = True)
    site = models.URLField(null = True)
class ITC_location(models.Model):
    itc = models.ForeignKey(ITC1, on_delete=models.CASCADE)
    location_kz = models.TextField(max_length = 1000, null = True)
    location_ru = models.TextField(max_length = 1000, null = True)
    location_en = models.TextField(max_length = 1000, null = True)
    map = models.URLField(null = True)
class ITC_about(models.Model):
    itc = models.ForeignKey(ITC1, on_delete=models.CASCADE)
    mode_kz = models.TextField(max_length = 1000, null = True)
    mode_ru = models.TextField(max_length = 1000, null = True)
    mode_en = models.TextField(max_length = 1000, null = True)
    tag_kz = models.TextField(max_length = 1000, null = True)
    tag_ru = models.TextField(max_length = 1000, null = True)
    tag_en = models.TextField(max_length = 1000, null = True)
#HOTEL
class Hotel_type(models.Model):
    type_kz = models.TextField(max_length = 1000, null = True)
    type_ru = models.TextField(max_length = 1000, null = True)
    type_en = models.TextField(max_length = 1000, null = True)
class Hotel_name(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
    class Meta:
        verbose_name = "Название отеля"
        verbose_name_plural = "Название отеля"
    def __str__(self):
        return self.name_ru
class Hotel(models.Model):
    region = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    district = models.ForeignKey(District_n, on_delete=models.CASCADE)
    #type = models.ForeignKey(Hotel_type, on_delete=models.CASCADE)
    name = models.ForeignKey(Hotel_name, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    value = models.IntegerField()
    site = models.URLField()
    photo = models.ImageField(null=True)
    class Meta:
        verbose_name = "Отели"
        verbose_name_plural = "Отели"
    def __str__(self):
        return self.hotel.name_ru
class Hotel_about(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    address_kz = models.TextField(max_length = 1000, null = True)
    address_ru = models.TextField(max_length = 1000, null = True)
    address_en = models.TextField(max_length = 1000, null = True)
    phone = models.TextField(max_length = 1000, null = True)
#RESTAURANT
class Kitchen_type(models.Model):
    type_kz = models.TextField(max_length = 1000, null = True)
    type_ru = models.TextField(max_length = 1000, null = True)
    type_en = models.TextField(max_length = 1000, null = True)
class Restaurant_type(models.Model):
    type_kz = models.TextField(max_length = 1000, null = True)
    type_ru = models.TextField(max_length = 1000, null = True)
    type_en = models.TextField(max_length = 1000, null = True)
class Restaurant_name(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
class Restaurant(models.Model):
    region = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    district = models.ForeignKey(District_n, on_delete=models.CASCADE)
    name = models.ForeignKey(Restaurant_name, on_delete=models.CASCADE)
    type = models.ForeignKey(Restaurant_type, on_delete=models.CASCADE)
    kitchen = models.ForeignKey(Kitchen_type, on_delete=models.CASCADE)
    avg_acc = models.FloatField()
    site = models.URLField()
    photo = models.ImageField(null=True)
class Restaurant_about(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    mode_kz = models.TextField(max_length = 1000, null = True)
    mode_ru = models.TextField(max_length = 1000, null = True)
    mode_en = models.TextField(max_length = 1000, null = True)
    address_kz = models.TextField(max_length = 1000, null = True)
    address_ru = models.TextField(max_length = 1000, null = True)
    address_en = models.TextField(max_length = 1000, null = True)
    phone = models.TextField(max_length = 1000, null = True)
#Terminus
class Direction_type(models.Model):
    type_kz = models.TextField(max_length = 1000, null = True)
    type_ru = models.TextField(max_length = 1000, null = True)
    type_en = models.TextField(max_length = 1000, null = True)
class Terminus_type(models.Model):
    type_kz = models.TextField(max_length = 1000, null = True)
    type_ru = models.TextField(max_length = 1000, null = True)
    type_en = models.TextField(max_length = 1000, null = True)
class Terminus_name(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
class  Terminus(models.Model):
    type = models.ForeignKey(Terminus_type, on_delete=models.CASCADE)
    region_name = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    city_name = models.ForeignKey(City_name, on_delete=models.CASCADE)
    name = models.ForeignKey(Terminus_name, on_delete=models.CASCADE)
    site = models.URLField()
    photo = models.ImageField()
class  Terminus_direction(models.Model):
    terminus = models.ForeignKey(Terminus, on_delete=models.CASCADE)
    type = models.ForeignKey(Direction_type, on_delete=models.CASCADE)
    route = models.TextField(max_length = 1000)
class  Terminus_about(models.Model):
    terminus = models.ForeignKey(Terminus, on_delete=models.CASCADE)
    address_kz = models.TextField(max_length = 1000, null = True)
    address_ru = models.TextField(max_length = 1000, null = True)
    address_en = models.TextField(max_length = 1000, null = True)
    phone = models.TextField(max_length = 1000, null = True)
    map = models.URLField()
#Airport
class Airport_name(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
class  Airport(models.Model):
    region_name = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    city_name = models.ForeignKey(City_name, on_delete=models.CASCADE)
    name = models.ForeignKey(Airport_name, on_delete=models.CASCADE)
    site = models.URLField()
    photo = models.ImageField()
class  Airport_direction(models.Model):
    airport = models.ForeignKey(Airport, on_delete=models.CASCADE)
    type = models.ForeignKey(Direction_type, on_delete=models.CASCADE)
    route = models.TextField(max_length = 1000)
class  Airport_about(models.Model):
    airport = models.ForeignKey(Airport, on_delete=models.CASCADE)
    address_kz = models.TextField(max_length = 1000, null = True)
    address_ru = models.TextField(max_length = 1000, null = True)
    address_en = models.TextField(max_length = 1000, null = True)
    phone = models.TextField(max_length = 1000, null = True)
    map = models.URLField()
#Taxi
class Taxi_name(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
class  Taxi(models.Model):
    region_name = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    city_name = models.ForeignKey(City_name, on_delete=models.CASCADE)
    name = models.ForeignKey(Taxi_name, on_delete=models.CASCADE)
    site = models.URLField()
    phone = models.TextField(max_length = 1000, null = True)
#My journey
EMOTIONS = (('1','1'),
          ('2','2'),
          ('3', '3'),
          ('4','4'),
          ('5','5'))
class  Author(models.Model):
   name  = models.TextField(max_length = 1000, null = True)
   account = models.TextField(max_length = 1000, null = True)
class  My_journey(models.Model):
    date = models.DateTimeField(auto_now=False)
    region_name = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    city_name = models.ForeignKey(City_name, on_delete=models.CASCADE)
    object_name = models.ForeignKey(Objects_name, on_delete=models.CASCADE)
    author_id = models.ForeignKey(Author, on_delete=models.CASCADE)
    text = models.TextField(max_length = 1000, null = True)
    emotions = models.TextField( max_length = 1000, choices=EMOTIONS)
    photo = models.ImageField()
    recommendation = models.BooleanField("Рекомендую", blank=True)
# Карта достопримечательностей
class  Showplaces_map(models.Model):
    name = models.TextField(max_length = 1000)
    object_name = models.ForeignKey(Objects_name, on_delete=models.CASCADE)
    map = models.URLField() #ID объекта карты
    object = models.ForeignKey(Object, on_delete=models.CASCADE)
class  Tours_map(models.Model):
    name = models.TextField(max_length = 1000)
    object_name = models.ForeignKey(Objects_name, on_delete=models.CASCADE)
    map = models.URLField() #ID объекта карты
    object_id = models.ForeignKey(Object, on_delete=models.CASCADE)
    url = models.URLField(null=True)
#TOUR
class Tour_category(models.Model):
    category_kz = models.TextField(max_length = 1000, null = True)
    category_ru = models.TextField(max_length = 1000, null = True)
    category_en = models.TextField(max_length = 1000, null = True)
class Tour_type(models.Model):
    type_kz = models.TextField(max_length = 1000, null = True)
    type_ru = models.TextField(max_length = 1000, null = True)
    type_en = models.TextField(max_length = 1000, null = True)
class Tour_name(models.Model):
    name_kz = models.TextField(max_length = 1000, null = True)
    name_ru = models.TextField(max_length = 1000, null = True)
    name_en = models.TextField(max_length = 1000, null = True)
class Tour(models.Model):
    category = models.ForeignKey(Tour_category, on_delete=models.CASCADE)
    type = models.ForeignKey(Tour_type, on_delete=models.CASCADE)
    name = models.ForeignKey(Tour_name, on_delete=models.CASCADE)
    region_name = models.ForeignKey(Region_name, on_delete=models.CASCADE)
    city_name = models.ForeignKey(City_name, on_delete=models.CASCADE)
    object = models.ForeignKey(Objects_name, on_delete=models.CASCADE)
    duration = models.TextField(max_length=100)
    map = models.URLField()
    recommendation_kz = models.TextField(null = True)
    recommendation_ru = models.TextField(null = True)
    recommendation_en = models.TextField(null = True)
