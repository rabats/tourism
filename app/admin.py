from django.contrib import admin
from app.models import *
#REGION
admin.site.register(Region)
#admin.site.register(Region_name)
admin.site.register(Region_borders)
#admin.site.register(Region_administration)
admin.site.register(Region_directory)
admin.site.register(Region_events)
admin.site.register(Region_weather)
  
@admin.register(Region_name)
class AdminRegion_name (admin.ModelAdmin):
    list_display = ["name_ru", "name_kz"]

@admin.register(Region_administration)
class AdminRegion_administration (admin.ModelAdmin):
    list_display = ["admin_ru", "admin_kz"]

 
    #District
admin.site.register(District_n)
admin.site.register(District)

    #Object
admin.site.register(Tourism_type)
#admin.site.register(Object_type)
#admin.site.register(Objects_name)
admin.site.register(Object)
admin.site.register(Objects_location)
admin.site.register(Objects_about)

@admin.register(Objects_name)
class AdminObjects_name (admin.ModelAdmin):
    list_display = ["name_ru", "name_kz"]

@admin.register(Object_type)
class AdminObject_type (admin.ModelAdmin):
    list_display = ["type_ru", "type_kz"]

